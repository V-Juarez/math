<!--
Created: Thu Jun 03 2021 16:37:54 GMT-0600 (Central Standard Time)
Modified: Thu Jun 03 2021 16:38:54 GMT-0600 (Central Standard Time)
-->

<h1>Fundamentos de MatemĂĄticas</h1>

<h3>Sergio Orduz</h3>

<h1>Tabla de Contenido</h1>

- [1. AritmĂŠtica](#1-aritmĂŠtica)
  - [IntroducciĂłn al curso](#introducciĂłn-al-curso)
  - [Operaciones bĂĄsicas](#operaciones-bĂĄsicas)
  - [PotenciaciĂłn y sus propiedades](#potenciaciĂłn-y-sus-propiedades)
      - [Tabla de Informacion de porque 0](#tabla-de-informacion-de-porque-0)
  - [RadicaciĂłn y sus propiedades](#radicaciĂłn-y-sus-propiedades)
    - [ÂżPor quĂŠ decimos raĂ­z cuadrada o un nĂşmero elevado al cuadrado?](#por-quĂŠ-decimos-raĂ­z-cuadrada-o-un-nĂşmero-elevado-al-cuadrado)
    - [ÂżY al cubo?](#y-al-cubo)
      - [Propiedades de la radicaciĂłn:](#propiedades-de-la-radicaciĂłn)
      - [Simplificar una raĂ­z:](#simplificar-una-raĂ­z)
  - [Orden de operaciones](#orden-de-operaciones)
  - [FactorizaciĂłn](#factorizaciĂłn)
  - [Recta numĂŠrica](#recta-numĂŠrica)
  - [ÂĄA practicar lo aprendido!](#a-practicar-lo-aprendido)
- [2. Principios del ĂĄlgebra](#2-principios-del-ĂĄlgebra)
  - [Principios del ĂĄlgebra](#principios-del-ĂĄlgebra)
  - [Tu primer reto](#tu-primer-reto)
  - [SimbologĂ­a en el ĂĄlgebra](#simbologĂ­a-en-el-ĂĄlgebra)
  - [Propiedades de las ecuaciones](#propiedades-de-las-ecuaciones)
  - [Orden de despeje](#orden-de-despeje)
  - [Despejando exponentes y raĂ­ces en ĂĄlgebra](#despejando-exponentes-y-raĂ­ces-en-ĂĄlgebra)
  - [ÂĄEs hora de practicar!](#es-hora-de-practicar)
- [3. Polinomios](#3-polinomios)
  - [Polinomios](#polinomios)
  - [Simplificando polinomios](#simplificando-polinomios)
  - [La propiedad distributiva de la multiplicaciĂłn](#la-propiedad-distributiva-de-la-multiplicaciĂłn)
  - [Desarrollando polinomios](#desarrollando-polinomios)
  - [Pon a prueba tus conocimientos y simplifica polinomios](#pon-a-prueba-tus-conocimientos-y-simplifica-polinomios)
- [4. Funciones](#4-funciones)
  - [ÂżQue es una funciĂłn?](#que-es-una-funciĂłn)
  - [TabulaciĂłn de funciones](#tabulaciĂłn-de-funciones)
  - [Plano Cartesiano](#plano-cartesiano)
  - [ÂżCuĂĄles de las siguientes son funciones?](#cuĂĄles-de-las-siguientes-son-funciones)
- [5. GrĂĄficas](#5-grĂĄficas)
  - [Test lĂ­nea vertical](#test-lĂ­nea-vertical)
  - [Funciones lineales](#funciones-lineales)
  - [Todo se puede graficar](#todo-se-puede-graficar)
  - [ÂĄEs hora de graficar!](#es-hora-de-graficar)
  - [CĂłmo identificar funciones lineales a partir de una ecuaciĂłn](#cĂłmo-identificar-funciones-lineales-a-partir-de-una-ecuaciĂłn)
  - [ÂżcuĂĄl es la ecuaciĂłn de las siguientes grĂĄficas?](#cuĂĄl-es-la-ecuaciĂłn-de-las-siguientes-grĂĄficas)
  - [No pares de aprender](#no-pares-de-aprender)
# 1. AritmĂŠtica

## IntroducciĂłn al curso

**Fundamentos de MatemĂĄticas** donde Sergio Orduz, tu profesor en este curso, te enseĂąara las bases matemĂĄticas como la aritmĂŠtica que te ayudara para desarrollar el pensamiento abstracto y lĂłgico.
Este curso te serĂĄ de gran utilidad, aunque no estĂŠs estudiando una ingenierĂ­a o algĂşn ĂĄrea relacionada con las matemĂĄticas ya que te ayudara a ver el mundo de una manera diferente.

* [Las matemĂĄticas nos hacen mĂĄs libres y menos manipulables](https://www.youtube.com/watch?v=BbA5dpS4CcI&feature=youtu.be)

## Operaciones bĂĄsicas

ÂżQuĂŠ es la aritmĂŠtica?
Es una rama de las matemĂĄticas que estudia los nĂşmeros y las operaciones que se pueden realizar con ellos. Dentro de las operaciones que podemos realizar con los nĂşmeros encontramos cuatro operaciones bĂĄsicas:

* **Suma:** representada por el signo `+` esta operaciĂłn se encarga de adicionar datos entre sĂ­.
```math
  5 + 2 = 7
```
* **Resta:** representada por el signo `-` esta operaciĂłn se encarga de substraer un nĂşmero de otro. Esta operaciĂłn es la operaciĂłn inversa de la suma.

```math
  7 - 2 = 5
```
* **MultiplicaciĂłn:** representada por los signos `x, * y ..` Esta operaciĂłn se puede ver como una suma repetida.

```math
  1. 5 * 2 = 100
  2. 5 . 2 = 10
  3. 5 x 2 = 10
```

* **DivisiĂłn:** representada por los signos `/, - y Ăˇ` Esta operaciĂłn consiste en formar grupos de elementos.

```math
  1. 10 Ăˇ 2 = 10
  2. 10 / 2 = 10
  3.  \frac{10}{2} = 5
```

## PotenciaciĂłn y sus propiedades

En la multiplicaciĂłn tenĂ­amos un numero que se sumaba el numero de veces que indica el operando. Mientras que en la potenciaciĂłn tenemos un numero que va a ser multiplicado la cantidad de veces que indique su exponente.

Podemos encontrar varias propiedades en las potencias, por ejemplo:

  + En la multiplicaciĂłn de dos potencias con misma base los exponentes se suman.
  + En la divisiĂłn de dos potencias con misma base los exponentes se restan.
  + Una potencia elevada a la cero da uno.
  + Una potencia cuya base es cero siempre va a dar cero, no importa el exponente.

```math
2^3 = 8   2 * 2 * 2 = 8 2 + 2 + 2 + 2 = 8
```

La **base** `(a)`: indica el numero que se multiplicara por si mismo.

El **exponente** `(n)`: indica la cantidad de veces que el numero se multiplicara por si mismo.

```math
1. a^n . a^n = a^n + ^n

2. 2^3 * 2^4 = 2^7

3. \frac{a^n}{a^m} = a^n-^m

4. \frac{2^4}{2^3} = 2^1
```

![](https://i.ibb.co/HBF9FVS/potencia.webp)

#### Tabla de Informacion de porque 0

| Operacion | Resultado |
| :-------: | :-------: |
| ^1   |   1   |
| ```math 0, 9^0, ^9 ```   |   ```math 0, 909 ```   |
| ```math 0, 8^0, ^8 ```   |   ```math 0, 863 ```   |
| ```math 0, 7^0, ^7 ```   |   ```math 0, 779 ```   |
| ```math 0, 6^0, ^6 ```   |   ```math 0, 736 ```   |
| ```math 0, 5^0, ^5 ```   |   ```math 0, 707 ```   |
| ```math 0, 4^0, ^4 ```   |   ```math 0, 693 ```   |
| ```math 0, 3^0, ^3 ```   |   ```math 0, 696 ```   |
| ```math 0, 2^0, ^2 ```   |   ```math 0, 724 ```   |
| 0, 001^0, ^0, ^0, ^1   |   0, 993   |


![](https://i.ibb.co/ZND4xzt/ponten.png)

## RadicaciĂłn y sus propiedades

La operaciĂłn inversa de la potenciaciĂłn es la radicaciĂłn, lo que va a hacer esta operaciĂłn es buscar el nĂşmero raĂ­z o base que se debe multiplicar la cantidad de veces que indique el Ă­ndice para obtener el radicando.

Si estamos buscando la raĂ­z de una multiplicaciĂłn podemos separar los tĂŠrminos en varias operaciones raĂ­z y multiplicarlas.

**Formula**

```math
\sqrt{x
```

```math
1. $$4.\sqrt{2

2. \sqrt{9}
3. 3.\sqrt{8}

<!-- **Ejercicio** -->
\sqrt{32} = 5.65
\sqrt{16 * 2} = \sqrt{16} \sqrt{2}
4.\sqrt{2}
```
**RadicaciĂłn:** Busca encontrar el nĂşmero raĂ­z, es decir, de donde vino el nĂşmero.

### ÂżPor quĂŠ decimos raĂ­z cuadrada o un nĂşmero elevado al cuadrado?

* Porque si en un cuadrado multiplicamos el nĂşmero que mide un lado por el que mide otro, obtenemos el area que es el nĂşmero que mide el lado elevado a dos (ya que cada lado en un cuadrado mide lo mismo), es decir, el nĂşmero al cuadrado.

### ÂżY al cubo?

-Si cada arista mide x, ya que cada arista mide lo mismo, para calcular el volumen haremos el largo por el alto por el fondo, es decir:`x * x * ` o lo que es lo mismo:`x^` y por eso lo leemos como $x $al cubo.

#### Propiedades de la radicaciĂłn:

![](https://i.ibb.co/p3LRr01/raiz.webp)

#### Simplificar una raĂ­z:

![](https://i.ibb.co/vDqx9wG/raiz1.webp)

* [CĂłmo usar las fĂłrmulas](https://wiki.geogebra.org/es/C%C3%B3digo_LaTeX_para_las_f%C3%B3rmulas_m%C3%A1s_comunes)

## Orden de operaciones

Las operaciones se deben resolver siguiendo un orden:

  + Las que se encuentren dentro de parĂŠntesis y corchetes.
  + Las operaciones de potencias y raĂ­z.
  + Las operaciones de multiplicaciĂłn y divisiĂłn.
  + Por Ăşltimo, las operaciones de suma y resta.

```math
2 * 5 + 2 * 4 - 8 + 4 / 2 = 22 | 26 | 9
```

![](https://i.ibb.co/hBWVK7X/orden.webp)

Ejemplos

```math
(3 + 6) * 2
9 * 2 = 18
```

```math
(3 + 2^2) * 2
(3 + 4) * 2
7 * 2 = 14
```

```math
20 + 12 / 2
20 + 6 = 26
```

```math
4 * 8 / 2
32 / 2 = 16
```

```math
2 * 5 + 2 * 4 - 8 + 4 / 2
10 + 8 - 8 + 2 
10 + 2 = 12
```

## FactorizaciĂłn

La factorizaciĂłn consiste en el proceso de encontrar factores. Para un numero X, los factores serĂĄn los nĂşmeros que al multiplicarse su resultado sea igual al nĂşmero X.

Para facilitar el proceso de factorizaciĂłn debemos utilizar los nĂşmeros primos.

Los nĂşmeros primos son aquellos nĂşmeros cuyos Ăşnicos factores son el mismo nĂşmero y el nĂşmero 1.

> La factorizaciĂłn es una tĂŠcnica que consiste en la descomposiciĂłn de una expresiĂłn matemĂĄtica (que puede ser un nĂşmero, una suma o resta, una matriz, un polinomio, etc.) en forma de producto.

![](https://i.ibb.co/g7K8qhW/fact.webp)

![](https://i.ibb.co/g7Xykkb/fact1.webp)

![](https://i.ibb.co/jbMvM8M/fact3.webp)

![](https://i.ibb.co/R2smKhx/fact4.webp)

## Recta numĂŠrica

Los seres humanos somos muy visuales, por ello se tuvo la necesidad de graficar lo que se media, asĂ­ naciĂł la recta numĂŠrica.

En la recta numĂŠrica se encuentran tanto los nĂşmeros positivos como negativos y en su centro se encuentra el cero. Mientras mĂĄs a la derecha se encuentre un nĂşmero en la recta numĂŠrica, mayor va a ser su valor.

Los signos + y - tienen propiedades muy importantes:

* Signos diferentes dan negativo, por ejemplo, sumar un signo negativo te va a dar negativo.
* Par de signos iguales dan positivo, por ejemplo, restar un signo negativo te va a dar positivo.

![](https://i.ibb.co/L5WTSJX/r1.gif)

![](https://i.ibb.co/P5Dgyrb/r2.jpg)

## ÂĄA practicar lo aprendido!

ÂĄEs hora de practicar lo aprendido!

Pero antes te dejamos la soluciĂłn de algunos ejercicios para que compruebes tus resultados:

```math
3+6*4=27
```

Recuerda que la multiplicaciĂłn tiene prioridad sobre la suma por lo tanto la debemos hacer primero

```math
123+4*2+(3+2)=136
```

Debemos resolver primero la operaciĂłn entre parĂŠntesis luego la divisiĂłn y multiplicaciĂłn de izquierda a derecha y finalmente las sumas, comprueba que tengas el mismo resultado

```math
8^4=8x8x8x8
```

La potenciaciĂłn es lo mismo que tener una multiplicaciĂłn repetida

```math
(3*4+22+(3-2))+1=36
```

Tenemos un parĂŠntesis grande y debemos fijar nuestra atenciĂłn allĂ­, despuĂŠs vemos otro parĂŠntesis dentro de este por lo que esta serĂĄ nuestra primer operaciĂłn luego seguirĂĄn los exponentes y terminamos de resolver todo nuestro parĂŠntesis grande

```math
8+(-7)= 8-7
```

Recuerda que sumar algo negativo, en realidad es restar o quitar

```math
(-6)-(-7)= -6 + 7
```

Si quitamos algo negativo estamos en realidad agregando algo positivo

```math
12* (-2)= -24
```

En la multiplicaciĂłn si tenemos un signo negativo nos da como resultado un signo negativo

ÂżCuĂĄles son los factores primos del nĂşmero `45`?

Puedes utilizar cualquiera de los mĂŠtodos vistos en clase y encontrarĂĄs que son : `3, 3, 5`

ÂżCuĂĄles son los factores primos del nĂşmero `36`?

ÂżCuĂĄl de los dos nĂşmeros es mayor, `-5` o `-3`?

Recuerda que entre mĂĄs valor, mĂĄs negativo, por lo tanto `-3 > -5`

ÂĄAhora sĂ­! pon a prueba tus conocimientos y dĂŠjanos saber tu respuesta en la secciĂłn de comentarios.

```math
(-6)-(-7)*(-8)+(-5) = -67

(93+33+(4-26))+(3-6*2)= 95

12* (-2)(-1)(-3)= -72

8+(-7)*33= - 223

147+(83+(5+25))= 260
```

Encuentra la factorizaciĂłn de nĂşmeros primos de los siguientes nĂşmeros:

```math
32, 84, 16, 19, 26
(2.2.2.2.2)
(2.2.3.7)
(2.2.2.2)
(19.1)
(2.13)
```

Encuentra todos los posibles grupos de factores, no necesariamente primos de los siguientes nĂşmeros:

```math
34, 81, 36
34 = (1.34), (17.2
81 = (9.9), (81.1
36 = (2.2.3.3), (36.1), (18.2), (3.4.3), (9.4), (9.2.2), (3.6.2
```

# 2. Principios del Ąlgebra

## Principios del Ąlgebra

Cuando tenemos una operaciĂłn cuyo resultado no sabemos, este resultado lo representaremos con la letra x y se llamara variable.

Una ecuaciĂłn es una igualdad entre dos expresiones que contienen una o mĂĄs variables.

Si tenemos dos ecuaciones distintas, sus valores de x van a ser distintos. Sin embargo, en un sistema de ecuaciones tendremos mĂĄs de dos ecuaciones cuyo valor de x serĂĄ igual.

En una ecuaciĂłn donde tenemos una misma variable x, no puede tomar valores distintos, en cambio si tenemos una variable x y una variable y estas pueden tener mismos valores y tambiĂŠn distintos.

El ĂĄlgebra (del ĂĄrabe: Ř§ŮŘŹŘ¨Řą al-Ĺˇabr 'reintegraciĂłn, recomposiciĂłnâ) es la rama de la matemĂĄtica que estudia la combinaciĂłn de elementos de estructuras abstractas acorde a ciertas reglas. Originalmente esos elementos podĂ­an ser interpretados como nĂşmeros o cantidades, por lo que el ĂĄlgebra en cierto modo originalmente fue una generalizaciĂłn y extensiĂłn de la aritmĂŠtica.2â3â En el ĂĄlgebra moderna existen ĂĄreas del ĂĄlgebra que en modo alguno pueden considerarse extensiones de la aritmĂŠtica (ĂĄlgebra abstracta, ĂĄlgebra homolĂłgica, ĂĄlgebra exterior, etc.).

A diferencia de la aritmĂŠtica elemental, que trata de los nĂşmeros y las operaciones fundamentales, en ĂĄlgebra -para lograr la generalizaciĂłn- se introducen ademĂĄs sĂ­mbolos (usualmente letras) para representar parĂĄmetros (variables o coeficientes), o cantidades desconocidas (incĂłgnitas); las expresiones asĂ­ formadas son llamadas ÂŤfĂłrmulas algebraicasÂť, y expresan una regla o un principio general.â El ĂĄlgebra conforma una de las grandes ĂĄreas de las matemĂĄticas, junto a la teorĂ­a de nĂşmeros, la geometrĂ­a y el anĂĄlisis.

```math
1 + 2 = x      |        3 + 2 = x
    x = 3      |            x = 5

12 / 2 = x
4 + 2 = x
```

El algebra es la rama de las matemĂĄticas que estudia los nĂşmeros, sus propiedades y de la estructura/conformaciĂłn de los conjuntos numĂŠricos en relaciĂłn con las propiedades establecidas sobre ellos, utilizando la âabstraccionâ mediante el uso de letras y simbolos que permiten representar y construir expresiones algebraicas.

EcuaciĂłn:Igualdad entre dos expresiones que contiene una o mĂĄs variables.

- [Algebra](https://www.thatquiz.org/es-0/matematicas/algebra/)
- [Learn to think](https://brilliant.org/)

## Tu primer reto

```math
(4x - 5 / 2)^2 = 9
```

<img src="https://i.ibb.co/Jy86snw/retomath.jpg" alt="retomath" border="0">

## Simbologia en el Ąlgebra

En el ĂĄlgebra seguiremos usando la simbologĂ­a de la suma y resta, pero para la divisiĂłn y multiplicaciĂłn habrĂĄ unos ligeros cambios. Para la divisiĂłn ya no se usarĂĄ tanto el sĂ­mbolo Ăˇ, en su lugar usaremos una lĂ­nea -. La multiplicaciĂłn seguirĂĄ teniendo los sĂ­mbolos x, . y *, pero se le va a aĂąadir el poder representar una multiplicaciĂłn encerrando sus factores en parĂŠntesis: `(2)(5) = 10`

<img src="https://i.ibb.co/LxkhGBN/algebraico.jpg" alt="algebraico" border="0">

SĂ­mbolos del ĂĄlgebra:

SUMA, signo: +
RESTA, signo: -
DIVISĂN, signo: _____

```math
Ej: 5x+3-2
2y

<!-- MULTIPLICACIĂN, Signos: -->
5x4=20
5.4=20
5*4=20
(5)(4)=20
5X = 20 (X equivale a 4)
```

## Propiedades de las ecuaciones

Si una ecuaciĂłn A es igual a una ecuaciĂłn B, y esta a su vez es igual a una ecuaciĂłn C, entonces A es igual a C.

Para mantener la igualdad dentro de una ecuaciĂłn si realizamos una operaciĂłn de un lado, debemos realizar la misma operaciĂłn del otro lado de la igualdad.

La igualdad en matemĂĄticas posee 5 propiedades:

1)propiedad reflexiva/identica: Establece que toda cantidad o expresiĂłn matemĂĄtica es igual a si misma: b = b a = a

2)Propiedad Simetrica:Establece que sin importar el orden de los miembros la igualdad prevalece: a + b = c c = a + b

3)Propiedad Transitiva: Si los miembros que conforman una igualdad tienen un elemento en comun, se dice que este elemento es igual a cualquiera de los 2 miembros de la igualdad: a = b b = c a = c
a = b a = c b = c

4)Propiedad Uniforme:Si aumenta/disminuye la misma cantidad de los miembros de una igualdad, esta prevalece: 4 + 3 = 7 4 + 3 + 2 = 7 + 2

Propiedad cancelativa:Al suprimir 2 elementos/cantidades iguales en ambos miembros la igualdad se mantiene: si 15 + 8 = 17 + 6 entonces si restamos 6 de ambos lados: 15 + 2 = 17

Como dijo el profesor. En esta ecuaciĂłn es diferente, porque el signo menos pertenece a la variable z
es decir:
4 - z = 9
Si resuelves como en los anteriores ejemplos:
4-4 -z = 9-4
tendrias que restarle 4 en los dos lados de la ecuacion
pero resulta que al hacerlo, la variable Z como respuesta queda negativa
-z = 5
Y tu variable en este caso no puede estar negativa

Por lo que el profesor propone cambiar la Z del lado de la ecuaciĂłn para asĂ­ poder cambiarle el signo.
Como el signo negativo esta pegado a la Z al sumarle otra Z lograriamos borrar esa Z de un lado pero se la aĂąadiriamos al otro lado. Seria como hacerla desaparecer de un lado para que aparezca en el otro
Entonces: 4 - z + z = 9 + z
Entonces queda 4 = 9 + z
Como ves, ahora ya no es un signo negativo el que esta junto a la Z sino un signo positivo

Ahora resolveriamos esta ecuacion dejando a la Z positiva completamente sola, para eso tendria que quitar al 9 de su lado. Como ese 9 es positivo, le resto otro 9 para que resulte en cero y desaparezca

4 = 9 - 9 + z
pero tendrĂ­a que restar ese mismo 9 en el primer lado tambiĂŠn
4 - 9 = 9 - 9 + z
-5 = z
Ahora como ves el signo negativo quedĂł en el nĂşmero y no en la variable Z.

Esto lo puedes cambiar de lugar y es lo mismo
-5 = z es igual que decir z= -5

El profesor al final no da la respuesta y la respuesta es que Z es igual a -5

z= -5

## Orden de despeje

Vamos a resolver nuestras primeras ecuaciones, para ello debemos seguir un orden. AsĂ­ como en aritmĂŠtica empezĂĄbamos resolviendo los parĂŠntesis y terminabas con las sumas, pero en una ecuaciĂłn vamos a realizar el proceso al revĂŠs, iniciando por las sumas y terminando en los parĂŠntesis.


Vamos a resolver nuestras primeras ecuaciones, para ello debemos seguir un orden. AsĂ­ como en aritmĂŠtica empezĂĄbamos resolviendo los parĂŠntesis y terminabas con las sumas, pero en una ecuaciĂłn vamos a realizar el proceso al revĂŠs, iniciando por las sumas y terminando en los parĂŠntesis.

```math
<!-- Ecuaciones -->
4x + 3 = 16
4x + 3 - 3 = 16 -3
4x = 13
(4x) / 4 = 13/4
x = 13/4

```

## Despejando exponentes y raices en Ąlgebra

En esta clase veremos como despejar una potencia o raĂ­z de una ecuaciĂłn.
Cuando tenemos una raĂ­z cuadrada de una variable en nuestra ecuaciĂłn, solamente debemos elevar al cuadrado cada lado de la igualdad para obtener la soluciĂłn.
En los casos que tenemos una variable elevada al cuadrado debemos aplicar raĂ­z cuadrada a ambos lados de la igualdad, pero el resultado de la variable se deberĂĄ mostrar tanto positivo como negativo.

<img src="https://i.ibb.co/r2QFkRZ/desppejando-exponentes-raiz-algebra.jpg" alt="desppejando-exponentes-raiz-algebra" border="0">

<img src="https://i.ibb.co/tsDrKcC/operaciones.jpg" alt="operaciones" border="0">

<img src="https://i.ibb.co/L5F54sF/Exponentes-fraccionarios-d520e691-ce33-459d-a948-782eb84ad8a2.jpg" alt="Exponentes-fraccionarios-d520e691-ce33-459d-a948-782eb84ad8a2" border="0">

## ÂĄEs hora de practicar!

Despeja las siguientes ecuaciones y encuentra el valor de x

<img src="https://i.ibb.co/z5K4ZCC/reto2.jpg" alt="reto2" border="0">

ÂĄAhora sĂ­! pon a prueba tus conocimientos y dĂŠjanos saber tu respuesta en la secciĂłn de comentarios.

<img src="https://i.ibb.co/MpFNvdV/resultados-reto2.jpg" alt="resultados-reto2" border="0">

# 3. Polinomios

## Polinomios

**ÂżQuĂŠ es un polinomio?**
La palabra poli significa mĂşltiples o varios, mientras que nomio significa tĂŠrmino, entonces polinomio son varios tĂŠrminos. Un tĂŠrmino es el conjunto de un coeficiente y una variable.

El grado de un polinomio estarĂĄ determinado por el grado mĂĄximo de los exponentes. Mientras mĂĄs grande sea el grado de un polinomio mayor serĂĄ su complejidad.

<img src="https://i.ibb.co/SdnsVB2/polinimio.jpg" alt="polinimio" border="0">

Existen dos conceptos cuando en una ecuaciĂłn aparecen 2 variables a mĂĄs: Grado Absoluto (GA) y Grado Relativo (GR).

- El GA es el que explicĂł el profesor en esta clase: Sumar los exponentes de cada tĂŠrmino.
- El GR es el grado con respecto a una sola variable, es decir, tomar en cuenta solo los exponentes de dicha variable.

> Nota: Cuando un polinomio tiene mĂĄs de una variable, el grado del polinomio es la mayor de las sumas de los exponentes de cada tĂŠrmino. Ejemplo: xy+x+2 es de segundo grado; porque los exponentes de cada variable de xy suman 1+1=2, los de x suman 1 y los de 2 suman 0. El mayor de estos es dos.

- ÂżQue es un Polinomio?
  Un polinomio es una expresiĂłn hecha con constantes, variables y exponentes, que estĂĄn combinados usando sumas, restas y multiplicaciones, âŚ pero no divisiones.
  Los exponentes
  Los exponentes sĂłlo pueden ser 0,1,2,3,âŚ etc.
  No puede tener un nĂşmero infinito de tĂŠrminos.

**Estructura de los Polinomios**

<img src="https://i.ibb.co/wRhVqXC/polynomial.gif" alt="polynomial" border="0">

**Estructura de Monomio**

<img src="https://i.ibb.co/KXBb8GN/monomio.png" alt="monomio" border="0">

```mathematica
2x^3 + 3x^2 + 2x + 3
2x^3 3x^2 2x^1 3x^0
```

Aparte de tener clases de polinomios segĂşn su numero de tĂŠrminos como se vio en la clase, tambiĂŠn se pueden clasificar por segĂşn su grado:

- Polinomio Nulo: Cuando su grado es cero o su mayor exponente es 0.
- Polinomio de Primer grado: Cuando su termino lĂ­der tiene exponente 1.
- Polinomio de Segundo grado: Mayor exponente es 2.
- Polinomio de Tercer grado: Mayor exponente es 3.
  De aquĂ­ se pueden sacar:
  El termino independiente que es aquel que su variable esta elevada a la cero.

## Simplificando polinomios

Los tĂŠrminos semejantes son aquellos que tienen misma variable y exponente, por ello se pueden sumar o restar. Podemos simplificar los polinomios buscando unir sus tĂŠrminos semejantes.

<img src="https://i.ibb.co/tpzzt7X/simppol.jpg" alt="simppol" border="0">

> La constante tambien se denomina termino independiente.
>
> Si, porque en realidad serĂ­a 1x^0, donde 1 es el termino y la x^0 es la variable.

**Simplificacion de Terminos Semejantes:**

- Podemos operar (sumar) los terminos semejantes (que tengan la misma parte literal/variables)
- Las Matematicas tienden a simplificar todo (aunque no lo parezcan)
- El orden es muy importante para no cometer errores

## La propiedad distributiva de la multiplicacion

Al momento de simplificar polinomios nos serĂĄ de gran utilidad la propiedad distributiva. Esta propiedad en aritmĂŠtica nos permite solucionar de dos maneras la siguiente expresiĂłn `3*(4+6)` ya sea resolviendo primero la suma dentro del parĂŠntesis y luego multiplicar, o multiplicando el nĂşmero por cada componente de la suma y posteriormente sumar los resultados.

En ĂĄlgebra en una expresiĂłn `3(x+6)` no podemos sumar 6 a x, por ello hacemos uso de la propiedad distributiva para multiplicar 3 por cada uno de los componentes de la suma, dejando la siguiente expresiĂłn `3x+18`.

> **Propiedades conmutativas**
> *a + b = b + a
> ab = ba*
>
> **Propiedades asociativas**
> *(a + b) + c = a + (b + c)
> (ab)c = a(bc)*
>
> **Propiedad distributiva**
> *a(b + c) = ab + ac
> (b + c)a = ab + ac*

La propiedad distributiva es algo bastante Ăştil, no sĂłlo en el ĂĄlgebra sino tambiĂŠn en la aritmĂŠtica que se ocupa de manera cotidiana.
Si tienes que resolver mentalmente una multiplicaciĂłn, por ejemplo 4 * 18, es mĂĄs sencillo sacar: 4 * 10 = 40 y 4 * 8 = 32 y despuĂŠs sumarlos para obtener 72, que si haces mentalmente la operaciĂłn de 4*18.
Esta forma de multiplicar de verdad que agiliza mucho el pensamiento, yo les recomiendo que lo hagan hasta un hĂĄbito en su vida cotidiana,verĂĄn cĂłmo les simplifica la vida y les ayuda a desarrollar diferentes habilidades cognitivas.

```mathematica
6xÂ˛y - 16xÂ˛yÂ˛ + 12yÂ˛x
en
(6xÂ˛ - 16xÂ˛) + (-16yÂ˛ + 12yÂ˛) + 6y + 12x,
que simplificado quedarĂ­a
-10xÂ˛ - 4yÂ˛ + 12x + 6y
```

<img src="https://i.ibb.co/3zF0Zq8/Propiedaddistributiva.jpg" alt="Propiedaddistributiva" border="0">

<img src="https://i.ibb.co/ZBRsm7J/multiplicacion.jpg" alt="multiplicacion" border="0">

## Desarrollando polinomios

**soluciĂłn:**

<img src="https://i.ibb.co/2FqKMt9/desarrollando-polinimios.jpg" alt="desarrollando-polinimios" border="0">

<img src="https://i.ibb.co/Ksr6JBG/Desarrollandopolinomios.jpg" alt="Desarrollandopolinomios" border="0">

## Pon a prueba tus conocimientos y simplifica polinomios

Pon a prueba tus conocimientos y dĂŠjanos saber cĂłmo simplificarĂ­as los siguientes polinomios:

<img src="https://i.ibb.co/94ZKvhj/simplificandopolinomios.jpg" alt="simplificandopolinomios" border="0">



**Solucion**

<img src="https://i.ibb.co/bHfMPNq/solucion.jpg" alt="solucion" border="0">

# 4. Funciones

## Que es una funcion?

Para una funciĂłn tendremos un valor de entrada y esta nos regresara un valor de salida diferente o igual, de acuerdo con lo que la funciĂłn indique. Por cada valor de entrada va a haber solamente un valor de salida. Los datos de entrada de una funciĂłn son llamados **dominio**, mientras que los datos de salida son llamados **rango**.

Al momento de escribir funciones podemos usar la variable y, pero una forma mĂĄs acadĂŠmica serĂ­a escribiendo **f (x)**.

> DefiniciĂłn de funciĂłn: âUna funciĂłn es una correspondencia entre dos conjuntos de forma que a cada elemento del conjunto inicial (variable independiente) le corresponda un Ăşnico elemento del conjunto final (variable dependiente)â

En programaciĂłn podemos decir que las funciones son muy similares a las de matemĂĄticas, cuando estas son invocadas, pueden recibir un Input(valores de entrada) y dar como respuesta un Output(valores de salida) que son el resultado de una serie de procesos por los que pasa el Input.

Por ejemplo, a la funciĂłn podemos mandarle un 3, que serĂ­a el input y que a ese input se le restase 2 por ejemplo y, entonces, la salida serĂ­a 1.

**AsĂ­ se verĂ­a en JavaScript:**

```js
function restarDos (input){
	return input -2;
}

restarDos (3) //Le mandamos un 3 al que le resta dos y nos devuelve 1 (El OutPut)
```

<img src="https://i.ibb.co/6RGv6xT/function.jpg" alt="function" border="0">

PodĂŠis verlo asĂ­ el funcionamiento de una funciĂłn.

AdemĂĄs, **una magnitud**(propiedad de un cuerpo que puede ser medida) **es funciĂłn de otra si el valor de la primera depende del valor de la segunda**.
Por ejemplo el ĂĄrea A de un cĂ­rculo es funciĂłn de su radio r, ya que si aumenta el radio, tambiĂŠn aumenta proporcionalmente el ĂĄrea.

<img src="https://i.ibb.co/q558ZzC/1.jpg" alt="1" border="0">

<img src="https://i.ibb.co/9qkyYh5/2.jpg" alt="2" border="0">

<img src="https://i.ibb.co/T2YxV3W/3.png" alt="3" border="0">



1. Se llama **dominio** a lo que puede entrar en una funciĂłn.
2. Se llama **codominio** a lo que es posible que salga de una funciĂłn.
3. Se llama **rango** o **imagen** a lo que en realidad sale de una funciĂłn.

Funciones:

- **Regla** que asigna a un elemento de un conjunto (Dominio) un **unico** elemento de otro conjunto que depende del anterio (Rango)
- Podemos ver a lo funcion como una âcaja negraâ que transforma los valores de entrada en valores de salida

**Notacion:**

```js
y=<expresion de x>
f(x)=<expresion de x>
y=variable dependiente
x=variable independiente
f(x): <nombre_funcion>(entrada) =salida
```

- La funciĂłn es como una caja mĂĄgica que por dentro realiza un proceso que nunca cambia, al cual le ingresas un **input** y te arroja un **output**, el cual es el resultado.
- El proceso interno de la caja **nunca cambia**, siempre serĂĄ el mismo (A menos que tu lo cambies claro). En cambio, el input y el output **puede cambiar**. Sin embargo hay que aclarar que existe una relaciĂłn de una sola vĂ­a entre el input y el output. El esclavo eterno âoutputâ siempre cambiarĂĄ en funciĂłn del autoritario âinputâ. Por eso tambiĂŠn se les llama **funciones**.

## TabulaciĂłn de funciones

Dentro de las funciones tenemos una variable dependiente e independiente. La variable independiente por lo general es *x* pues es el valor que le damos de entrada, puede ser cualquier valor, mientras que en la variable dependiente su valor serĂĄ igual al resultado de la funciĂłn.

Se considera asĂ­ en programaciĂłn, por ejemplo, en Javascript?
Entrada = input
Salida = output

**Una funciĂłn debe de tener una sola salida**, ya que si no serĂ­a imposible graficarla en un plano. *Recordemos, a cada valor de entrada le corresponde un Ăşnico valor de salida.*

### **TabulaciĂłn de funciones**

Recordemos que en una funciĂłn tenemos** la variable independiente (x, quĂŠ es donde ingresan los datos)** y **la variable dependiente (y, quĂŠ son los valores arrojados luego de que se ingresĂł x)**. Entonces, con las funciones podemos hacer una **tabla** en *dĂłnde ingresamos valores* para **x**, obteniendo resultados en **y**.

<img src="https://i.ibb.co/T0N5Ln9/Tabulaci-n-de-funciones.jpg" alt="Tabulaci-n-de-funciones" border="0">

> La **Y** en tĂŠrminos mas tĂŠcnicos vendrĂ­a siendo **f(x)**. Ya tu sabeâ *Att: el profe*

**Tabulacion de Funciones:**

- Representacion de algunos valores de la funcion en una tabla
- Identificamos las variables dependientes e independientes y aplicamos la regla de la funcion a la variable independiente (obteniendo la variable dependiente)
- Representamos nuestra tabla don estos valores; recuerda que a cada valor de la **variable independiente** le corresponde un **unico valo**r de la **variable dependiente**

## Plano Cartesiano

TambiĂŠn podemos graficar las funciones, asĂ­ como graficamos un nĂşmero dentro de la recta numĂŠrica para graficar una funciĂłn haremos uso de dos rectas numĂŠricas. Una recta de forma horizontal representando los valores de *x* y una recta vertical representando los valores de *y*. Esto es el plano cartesiano.

El plano cartesiano se encuentra dividido en cuatro cuadrantes. O tambiĂŠn es conocido como eje de las abscisas(x) y ordenadas (y)

<img src="https://i.ibb.co/nPR8TZS/halloween-arte-cartesiano-murcielago-pin.jpg" alt="halloween-arte-cartesiano-murcielago-pin" border="0">

> Cuadrante I: *x* y *y* son positivos.
> Cuadrante II: *x* es negativo y *y* es positivo.
> Cuadrante III: *x* y *y* son negativos.
> Cuadrante IV: *x* es positivo y *y* es negativo.

### **Plano cartesiano**

El plano cartesiano representa a la **variable independiente (x)** y la **dependiente (y)**.

<img src="https://i.ibb.co/nCKGQ5f/1.jpg" alt="1" border="0">



<img src="https://i.ibb.co/q5gk4by/2.jpg" alt="2" border="0">

### **ÂżCĂłmo graficar en el plano cartesiano?**

- A la hora de ubicar una funciĂłn, tenemos que tener en cuenta que cuando nos encontremos con cosas como **(3, 4)** significa que el primer nĂşmero que estĂĄ es **X**, mientras que el segundo es **Y**, por lo que 3 es **X **y 4 es **Y**.
- Teniendo en cuenta esto, vamos con un ejemplo. Tenemos la funciĂłn y = 2x junto a la tabla, por lo que podemos empezar a graficar los primeros puntos:

<img src="https://i.ibb.co/47FLkzm/4.jpg" alt="4" border="0">

**Ahora, tras marcar todos los puntos, podemos trazar una lĂ­nea que atraviese todos los puntos, formando una lĂ­nea. Esto se conoce como funciĂłn lineal:**

<img src="https://i.ibb.co/3Wfvkhx/3.jpg" alt="3" border="0">

### **Apunte extra:**

- **Existe aglo llamado cuadrantes, quĂŠ es lo que divide en 4 partes al plano cartesiano. AquĂ­ tienes cada cuadrante por su orden correspondiente:**

  

<img src="https://i.ibb.co/YLN09GB/5.jpg" alt="5" border="0">

## Cuales de las siguientes son funciones?

ÂżCuĂĄles de las siguientes son funciones? ÂĄTe invito a realizar una tabla o graficarlas y averiguarlo! No dejes de compartirnos tu respuesta

<img src="https://i.ibb.co/t2p873Z/funciones.jpg" alt="funciones" border="0">

<img src="https://i.ibb.co/5F88KV5/3.jpg" alt="3" border="0">

<img src="https://i.ibb.co/M5gZ3Gz/2.jpg" alt="2" border="0">

<img src="https://i.ibb.co/tQrcQr7/1.jpg" alt="1" border="0">

# 5. Graficas

## Test lĂ­nea vertical

Recuerda que en una funciĂłn por cada valor de *x* hay un valor de *y*. Para saber si tenemos una funciĂłn o no solamente debemos realizar la prueba de lĂ­nea vertical donde si un valor de *x* se cruza dos veces con un valor de *y*, entonces no es una funciĂłn.

El test de lĂ­nea vertical ayuda a identificar si una grĂĄfica es una funciĂłn de manera rĂĄpida y sencilla. Si al trazar una lĂ­nea imaginaria de manera vertical por cualquier parte de la grĂĄfica y esa lĂ­nea toca dos puntos, sencillamente no es un funciĂłn.

**Recordar:** Una funciĂłn tiene una entrada y una salida.

<img src="https://i.ibb.co/fX65Gn9/f.jpg" alt="f" border="0">

La linea vertical sobre el eje (x) nos ayuda a determinar cuando es una funciĂłn o no es una funciĂłn. TambiĂŠn se define cuando tiene una entrada y una sola salida, como una funciĂłn. Si tiene una entrada dos salidas no es una funciĂłn.

Test de linea Vertical:

- Recueda que a cada valor de X le corresponde un unico valor de Y
- Podemos reconocer las finciones graficas si trazamos una recta vertical (en cualquier punto) y esta **interseca a la funcion solo en un punto**

> Una funciĂłn tiene una entrada y una salida y las lĂ­neas imaginarias (lĂ­neas verticales) solo pueden tocar una sola vez la lĂ­nea de la funciĂłn.

## Funciones lineales

Las funciones lineales las vamos a utilizar y ver de forma frecuente en nuestra vida cotidiana, estas funciones tienen la forma ***y\* = \*mx\*** donde *m* se llama pendiente. La pendiente nos sirve para ver quĂŠ tan rĂĄpido crece o decrece nuestros valores de la funciĂłn.

Mientras mĂĄs grande sea el valor de la pendiente mĂĄs se va a acercar a la lĂ­nea vertical, pero nunca la va a tocar. Por otro lado, si nuestra pendiente tiene un valor menor a 1 y mayor a 0, se irĂĄ acercando a la lĂ­nea horizontal del plano cartesiano.

Las **funciones lineales** son de la forma **y = mx**, donde m es la pendiente, que indica que tan rapido crece algo.

Entre mas grande sea la pendiente (m), la recta se acercarĂĄ cada vez mas al eje y, lo que quiere decir que tenderĂĄ a ser vertical, mientras que entre mas pequeĂąa se la pendiente (m), la recta se acercarĂĄ cada vez mas al eje x, lo que quiere decir que tenderĂĄ a ser horizontal.

cuando la pendiente es positiva. Con pendiente negativa sucedería lo contrario, es decir, con una pendiente negativa menor la gráfica tenderá a acercarse cada vez más al eje y, mientras que con una pendiente negativa mayor la gráfica tenderá al eje de las x.

0 a 1 sea negativo o positivo se acerca mas a x y mayor a 1 negativo o positivo se acerca mas a y.

> Y = M x + H
>
> M = la pendiente de mi recta
> h= la ordenada al origen (donde corta al eje Y)
>
> Curiosidad, por quĂŠ en la expresiĂłn **y = mx + b**; b indica el punto de corte en el eje y?
>
> Para hallar el corte con el eje y lo hacemos cuando x es igual a 0 , y cuando esto pasa, el tĂŠrmino m * x queda todo igual a 0. Entonces la ecuaciĂłn queda y=b đŽ đ đ

 [GeoGebra](https://www.geogebra.org/graphing?lang=es) 👌 😃

<img src="https://i.ibb.co/D4HgQkW/2.jpg" alt="2" border="0">

Como dijo el profesor la ecuación que tiene una pendiente muy grande nunca llega a tocar el eje vertical pero si la pendiente vale 0 sí toca el eje horizontal.

Ampliación de la gráfica para ver la ecuación y = 200x porque en la imagen anterior parece que si toca el eje vertical:

<img src="https://i.ibb.co/gmTmkQv/1.jpg" alt="1" border="0">

## Todo se puede graficar

Si los valores de nuestra pendiente son negativos, entonces nuestra función tendrá valores en los cuadrantes II y IV.

Hasta el momento todas nuestras funciones han pasado por el punto (0, 0), si queremos que no pasen por este punto solamente necesitamos que a nuestra función se le sume una constante.

Para hallar el valor de la pendiente conociendo dos puntos de la recta se usa la siguiente fórmula:

<img src="https://i.ibb.co/QYdz70F/graficar.jpg" alt="graficar" border="0">

**Pero… ¿qué significa esta fórmula?**
Como lo explicó el profesor, la pendiente me indica cuánto va ha variar un valor con respecto a otro: Mientras mayor sea la pendiente, mayor será su variación.

Primero veamos cómo se relacionan los ejes o qué significan los ejes:
El eje de la abscisas (eje x) nos indica en qué “momento” de la gráfica estamos, mientras que el eje de las ordenadas (eje y) nos indica el valor de la función en dicho momento (valor de x).

<img src="https://i.ibb.co/pWGS1Gv/graficar1.jpg" alt="graficar1" border="0">

Por lo tanto, hallando cuánto varía el **valor de la función** con respecto a los **momentos** en los que analizamos la función, estaremos hallando la pendiente.

Para que nuestra función se represente en los cuadrantes II y IV los valores de su pendiente deberán ser negativos.

- Cada vez que hacemos más negativo el valor de la pendiente el decrecimiento será más pronunciado (más vertical), por ejemplo: *y = -100x*
- pero si tenemos valores menos negativos, por ejemplo: *y = -(1/2)x* el decrecimiento será más leve.

<img src="https://i.ibb.co/nM9TNkm/graf2.jpg" alt="graf2" border="0">

Con la función *y = mx* todas las líneas pasan por el punto 0 (0,0)

Pero si queremos que la línea no pase por el punto 0 → *y = mx + b*
(dónde la constante *b* será el valor por donde se desplaza en el eje y)

<img src="https://i.ibb.co/n70HSWH/graf1.jpg" alt="graf1" border="0">
<img src="https://i.ibb.co/1bBv5zL/traslaciones-3.jpg" alt="traslaciones-3" border="0">

<img src="https://i.ibb.co/5YgFxmK/traslaciones-5.jpg" alt="traslaciones-5" border="0">

## ¡Es hora de graficar!

Es hora de practicar graficando las siguientes funciones, ¿cuáles de ellas son líneas?, ¿que pasa cuando tenemos exponentes?

Todas son funciones polinómicas.
a) Son lineales los ejercicios 1, 2 y 5 (exponente de la variable independiente es 1), también conocida como función Afín.
b) No son lineales los ejercicios 3 (cuadrática) y 4 (cúbica); esto porque el exponente de la variable independiente es distinto a 1 y sus gráficas no son una línea, más bien curvas.
Saludos.

<img src="https://i.ibb.co/Ltjj3BP/FUNCIONLINEAL.jpg" alt="FUNCIONLINEAL" border="0">



## Cómo identificar funciones lineales a partir de una ecuación

Identificar si una función no es lineal a partir de su ecuación es bastante fácil, si la ecuación tiene algún exponente al cuadrado entonces ya no es una función lineal.

> Para identificar una función lineal a partir de una ecuación esta tiene que ser de primer grado (los polinomios que forman ambos miembros no superan el grado 1). Por eso se las llama también **ecuaciones lineales**

y = mx + b
y = variable dependiente
x = variable independiente
m = pendiente
b = punto de corte en el eje y

<img src="https://i.ibb.co/HGL6S38/Figura-6-Funciones-polinomiales-de-grado-1-2-y-3.png" alt="Figura-6-Funciones-polinomiales-de-grado-1-2-y-3" border="0">

<img src="https://i.ibb.co/ySxj93C/1.png" alt="1" border="0">

1. Están las funciones lineales que pasan siempre por cero, tienen solo un termino.
2. Están las funciones lineales que se desvían del punto cero, cuando tiene la suma de un termino.
3. Están las funciones cuadráticas que tienen exponente a la dos.
4. Esta la función cubica que que como su nombre lo dice esta a las tres.

## ¿Cuál es la ecuación de las siguientes gráficas?

<img src="https://i.ibb.co/T1nZzts/intuici-n.jpg" alt="intuici-n" border="0">

La ecuación de una recta está dada por: y = mx + b
donde:
m = pendiente de la recta
b = corte de la recta en y

a. Dado que, para un valor de x se tiene el mismo valor en y (y = x), y el corte de la gráfica en y = 2
La ecuación de la recta dibujada sería:

y = x + 2

También se puede comprobar de esta forma:
Cuando x = 0 y = 2
y cuando x = -2 y = 0

b. El corte en y es 1 y la pendiente es negativa
En principio, se puede plantear esta ecuación de la recta

y = -x + 1

Pero también se observa que hay un corte en x = 6 cuando y = 0
Para que esto se cumpla, sencillamente la pendiente debe ser igual a 1/6

Por lo tanto, la ecuación de la recta dibujada quedaría de esta forma:
y = -(1/6)x + 1

Comprobando que los cortes en x y y se cumplen para esta ecuación, sustituimos:
Cuando x = 0 y = 1
y cuando x = 6 y = 0

<img src="https://i.ibb.co/r<img src="https://i.ibb.co/rbt2F9J/Captura.jpg" alt="Captura" border="0">bt2F9J/Captura.jpg" alt="Captura" border="0">

## No pares de aprender

Has llegado al final de este curso
